library(readr)
library(ggplot2)
library( RWeka )
library(ggplot2)


set.seed(1810)
mushsample <- caret::createDataPartition(y = mushroom$edibility, times = 1, p = 0.8, list = FALSE)
train_mushroom <- mushroom[mushsample, ]
test_mushroom <- mushroom[-mushsample, ]

round(prop.table(table(mushroom$edibility)), 2)

round(prop.table(table(train_mushroom$edibility)), 2)


data(mushroom)
#tble <- table(mushroom[,c('color','edibility')])
#tble

#InformationGain(tble)


#InformationGain(table(mushroom[,c('size', 'edibility')]))

#InformationGain(table(mushroom[,c('points', 'edibility')]))

#InformationGain(table(mushroom[,c('odor', 'edibility')]))

gainRatioResult <- GainRatioAttributeEval( class ~ . , data = trainingData )
print( sort( gainRatioResult, decreasing = TRUE ))

ggplot(mushroom, aes(x = edibility, y = odor, col = edibility)) + 
  geom_jitter(alpha = 0.5) + 
  scale_color_manual(breaks = c("edible", "poisonous"), 
                     values = c("green", "red"))

#hist <- ggplot( mushroom, aes( x = odor, fill = class )) 
#hist <- hist + geom_bar(stat='count', position='dodge') + labs(x = 'Odor', y = 'Count of Class')
#hist

